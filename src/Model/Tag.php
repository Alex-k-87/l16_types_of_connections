<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

final class Tag extends Model
{

    public function posts()
    {
       return $this->belongsToMany(Post::class);
    }
}
